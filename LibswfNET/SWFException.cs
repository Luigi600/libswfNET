﻿namespace LibswfNET
{
    public class SWFException : System.Exception
    {
        private string error_message;

        public SWFException(string message = "swf_exception") : base()
        {
            this.error_message = message;
        }

        public override string Message
        {
            get => this.error_message;
        }
    }
}