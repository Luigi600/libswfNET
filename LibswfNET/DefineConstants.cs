﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LibswfNET
{
    internal static class DefineConstants
    {
        public const int SZ_OK = 0;
        public const int SZ_ERROR_DATA = 1;
        public const int SZ_ERROR_MEM = 2;
        public const int SZ_ERROR_CRC = 3;
        public const int SZ_ERROR_UNSUPPORTED = 4;
        public const int SZ_ERROR_PARAM = 5;
        public const int SZ_ERROR_INPUT_EOF = 6;
        public const int SZ_ERROR_OUTPUT_EOF = 7;
        public const int SZ_ERROR_READ = 8;
        public const int SZ_ERROR_WRITE = 9;
        public const int SZ_ERROR_PROGRESS = 10;
        public const int SZ_ERROR_FAIL = 11;
        public const int SZ_ERROR_THREAD = 12;
        public const int SZ_ERROR_ARCHIVE = 16;
        public const int SZ_ERROR_NO_ARCHIVE = 17;
#if !_WIN32
        public const int MY__FACILITY_WIN32 = 7;
#endif
        public const int True = 1;
        public const int False = 0;
#if _WIN32
	public const char CHAR_PATH_SEPARATOR = '\\';
#else
        public const char CHAR_PATH_SEPARATOR = '/';
#endif
#if _WIN32
	public const string STRING_PATH_SEPARATOR = "\\";
#else
        public const string STRING_PATH_SEPARATOR = "/";
#endif
        public const int LZMA_PROPS_SIZE = 5;
        public const int MP3D_SEEK_TO_BYTE = 0;
        public const int MP3D_SEEK_TO_SAMPLE = 1;
        public const int MP3D_SEEK_TO_SAMPLE_INDEXED = 2;

        //public static SortedDictionary<int, string> tagTypeNames = new SortedDictionary<int, string> {
        //        { -1, "File Header"},
		      //  { 0, "End"},
		      //  { 1, "ShowFrame"},
		      //  { 2, "DefineShape"},
		      //  { 3, "FreeCharacter"},
		      //  { 4, "PlaceObject"},
		      //  { 5, "RemoveObject"},
		      //  { 6, "DefineBitsJPEG"},
		      //  { 7, "DefineButton"},
		      //  { 8, "JPEGTables"},
		      //  { 9, "SetBackgroundColor"},
		      //  { 10, "DefineFont"},
		      //  { 11, "DefineText"},
		      //  { 12, "DoAction"},
		      //  { 13, "DefineFontInfo"},
		      //  { 14, "DefineSound"},
		      //  { 15, "StartSound"},
		      //  { 16, "StopSound"},
		      //  { 17, "DefineButtonSound"},
		      //  { 18, "SoundStreamHead"},
		      //  { 19, "SoundStreamBlock"},
		      //  { 20, "DefineBitsLossless"},
		      //  { 21, "DefineBitsJPEG2"},
		      //  { 22, "DefineShape2"},
		      //  { 23, "DefineButtonCxform"},
		      //  { 24, "Protect"},
		      //  { 25, "PathsArePostscript"},
		      //  { 26, "PlaceObject2"},
		      //  { 28, "RemoveObject2"},
		      //  { 29, "SyncFrame"},
		      //  { 31, "FreeAll"},
		      //  { 32, "DefineShape3"},
		      //  { 33, "DefineText2"},
		      //  { 34, "DefineButton2"},
		      //  { 35, "DefineBitsJPEG3"},
		      //  { 36, "DefineBitsLossless2"},
		      //  { 37, "DefineEditText"},
		      //  { 38, "DefineVideo"},
		      //  { 39, "DefineSprite"},
		      //  { 40, "NameCharacter"},
		      //  { 41, "ProductInfo"},
		      //  { 42, "DefineTextFormat"},
		      //  { 43, "FrameLabel"},
		      //  { 45, "SoundStreamHead2"},
		      //  { 46, "DefineMorphShape"},
		      //  { 47, "GenerateFrame"},
		      //  { 48, "DefineFont2"},
		      //  { 49, "GeneratorCommand"},
		      //  { 50, "DefineCommandObject"},
		      //  { 51, "CharacterSet"},
		      //  { 52, "ExternalFont"},
		      //  { 56, "Export"},
		      //  { 57, "Import"},
		      //  { 58, "EnableDebugger"},
		      //  { 59, "DoInitAction"},
		      //  { 60, "DefineVideoStream"},
		      //  { 61, "VideoFrame"},
		      //  { 62, "DefineFontInfo2"},
		      //  { 63, "DebugID"},
		      //  { 64, "EnableDebugger2"},
		      //  { 65, "ScriptLimits"},
		      //  { 66, "SetTabIndex"},
		      //  { 69, "FileAttributes"},
		      //  { 70, "PlaceObject3"},
		      //  { 71, "Import2"},
		      //  { 72, "DoABCDefine"},
		      //  { 73, "DefineFontAlignZones"},
		      //  { 74, "CSMTextSettings"},
		      //  { 75, "DefineFont3"},
		      //  { 76, "SymbolClass"},
		      //  { 77, "Metadata"},
		      //  { 78, "DefineScalingGrid"},
		      //  { 82, "DoABC"},
		      //  { 83, "DefineShape4"},
		      //  { 84, "DefineMorphShape2"},
		      //  { 86, "DefineSceneAndFrameData"},
		      //  { 87, "DefineBinaryData"},
		      //  { 88, "DefineFontName"},
		      //  { 90, "DefineBitsJPEG4"}
        //};
    }
}