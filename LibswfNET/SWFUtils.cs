﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ICSharpCode.SharpZipLib.Zip.Compression;
using ICSharpCode.SharpZipLib.Zip.Compression.Streams;

namespace LibswfNET
{
    /// <summary>The new clean version of SWF_utils. Will use in second release (or first version :P).</summary>
    /// <remarks>The most of functions use reference than parameter. The reason is not because there will edit them. Mostly, it is only for the perfomance!</remarks>
    public static class SWFUtils
    {
        /// <summary>Returns the twips unit in pixel unit.</summary>
        /// <param name="twips">The number in twips than unit.</param>
        /// <returns>The integer value that represents twips in pixel.</returns>
        public static int TwipsToPx(int twips) => twips / 20;

        #region SubBitArrays and SubBitSets
        /// <summary>Returns a part of a <c>BitArray</c>.</summary>
        /// <param name="ba"></param>
        /// <param name="seek">The start position in <paramref name="ba"/> which starts the result.</param>
        /// <param name="length">The length </param>
        /// <returns>A new <c>BitArray</c> that represents <paramref name="ba"/> in the range of <c><paramref name="seek"/></c> - <c><paramref name="seek"/> + <paramref name="length"/></c>.</returns>
        public static BitArray SubBitArray(ref BitArray ba, int seek, int length)
        {
            if (length > ba.Count)
                length = ba.Count;

            BitArray result = new BitArray(length);
            for (int i = ba.Length - 1 - seek,
                j = length - 1;
                i >= ba.Length - length - seek && j >= 0;
                i--, j--)
            {
                result[j] = ba[i];
            }

            return result;
        }

        /// <summary>Returns a part of a <c>BitSet</c>.</summary>
        /// <param name="bs"></param>
        /// <param name="startPos"></param>
        /// <param name="length"></param>
        /// <returns>A new <c>BitSet</c> that represents <paramref name="bs"/> in the range of <c><paramref name="seek"/></c> - <c><paramref name="seek"/> + <paramref name="length"/></c>.</returns>
        public static BitSet SubBitSet(ref BitSet bs, int startPos, int length)
        {
            if (length > bs.Length)
                length = bs.Length;

            BitSet result = new BitSet(length);
            for (int i = bs.Length - 1 - startPos,
                j = length - 1;
                i >= bs.Length - length - startPos && j >= 0;
                i--, j--)
            {
                result[j] = bs[i];
            }

            return result;
        }

        public static List<byte> SubBytes2List(ref byte[] bytes, int start, int end)
        {
            ICollection<byte> thanCollection = bytes;
            return SubBytes2List(ref thanCollection, start, end);
        }

        /// <summary>Returns a part of <c>ICollection</c> than <c>List</c>.</summary>
        /// <param name="bytes"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public static List<byte> SubBytes2List(ref ICollection<byte> bytes, int start, int end)
        {
            if (end > bytes.Count || end < start)
                end = bytes.Count;
            if (start < 0 || start >= bytes.Count)
                start = 0;

            List<byte> result = new List<byte>();
            for (int i = start; i < end; ++i)
                result.Add(bytes.ElementAt(i));

            return result;
        }

        public static byte[] SubBytes(ref byte[] bytes, int start, int end)
        {
            ICollection<byte> thanCollection = bytes;
            return SubBytes(ref thanCollection, start, end);
        }

        public static byte[] SubBytes(ref List<byte> bytes, int start, int end)
        {
            ICollection<byte> thanCollection = bytes;
            return SubBytes(ref thanCollection, start, end);
        }

        //Why not using of the another SubBytes and calls ToArray() on the return? -> PERFORMANCE! The bytes arrays are bigger. So RIP RAM when clone a list to generate a part of this clone.
        /// <summary>Returns a part of <c>ICollection</c> than array of <c>byte</c>.</summary>
        /// <param name="bytes"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public static byte[] SubBytes(ref ICollection<byte> bytes, int start, int end)
        {
            if (end > bytes.Count || end < start)
                end = bytes.Count;
            if (start < 0 || start >= bytes.Count)
                start = 0;

            byte[] result = new byte[end - start];
            for (int i = start; i < end; ++i)
                result[i - start] = bytes.ElementAt(i);

            return result;
        }
        #endregion

        #region Bytes to Number
        //REMARK: BitConverter is clever and knows that the input file is Little Endian
        public static uint BytesToUInt_LittleEndian(ref byte[] bytes, int seek) => BitConverter.ToUInt32(bytes, seek);
        public static int BytesToInt_LittleEndian(ref byte[] bytes, int seek) => BitConverter.ToInt32(bytes, seek);
        public static ushort BytesToUShort_LittleEndian(ref byte[] bytes, int seek) => BitConverter.ToUInt16(bytes, seek);
        public static byte BytesToByte_LittleEndian(ref byte[] bytes, int seek) => bytes[seek];

        public static ushort BytesToUShort_BigEndian(ref byte[] bytes, int seek)
        {
            ushort result = 0;
            for (int i = 0; i < sizeof(ushort); ++i)
                result = (ushort)(result | bytes[seek++] << ((sizeof(ushort) - i - 1) * 8));

            return result;
        }

        public static ushort BytesToUShort_BigEndian(ref ICollection<byte> bytes, int seek)
        {
            ushort result = 0;
            for (int i = 0; i < sizeof(ushort); ++i)
                result = (ushort)(result | bytes.ElementAt(seek++) << ((sizeof(ushort) - i - 1) * 8));

            return result;
        }

        public static uint BytesToUInt_BigEndian(ref byte[] bytes, int seek)
        {
            uint result = 0;
            for (int i = 0; i < sizeof(uint); ++i)
                result = (result | (uint)bytes[seek++] << ((sizeof(uint) - i - 1) * 8));

            return result;
        }
        #endregion

        #region Number to Bytes
        public static byte[] UIntToBytes_LittleEndian(uint data) => BitConverter.GetBytes(data);
        public static byte[] IntToBytes_LittleEndian(int data) => BitConverter.GetBytes(data);
        public static byte[] ShortToBytes_LittleEndian(short data) => BitConverter.GetBytes(data);
        public static byte[] UShortToBytes_LittleEndian(ushort data) => BitConverter.GetBytes(data);
        
        public static List<byte> UIntToListByte_LittleEndian(uint data) => new List<byte>(BitConverter.GetBytes(data));
        public static List<byte> IntToListByte_LittleEndian(int data) => new List<byte>(BitConverter.GetBytes(data));
        public static List<byte> ShortToListByte_LittleEndian(short data) => new List<byte>(BitConverter.GetBytes(data));
        public static List<byte> UShortToListByte_LittleEndian(ushort data) => new List<byte>(BitConverter.GetBytes(data));

        public static byte[] ShortToBytes_BigEndian(short data) => BitConverter.GetBytes(data).Reverse().ToArray();
        public static byte[] UShortToBytes_BigEndian(ushort data) => BitConverter.GetBytes(data).Reverse().ToArray();
        public static byte[] ULongToBytes_BigEndian(ulong data) => BitConverter.GetBytes(data).Reverse().ToArray();
        public static byte[] DoubleToBytes_BigEndian(double data) => BitConverter.GetBytes(data).Reverse().ToArray();
        public static byte[] UIntToBytes_BigEndian(uint data) => BitConverter.GetBytes(data).Reverse().ToArray();
        public static byte[] IntToBytes_BigEndian(int data) => BitConverter.GetBytes(data).Reverse().ToArray();

        /// <summary></summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static List<byte> ShortToListByte_BigEndian(short data) => new List<byte>(BitConverter.GetBytes(data).Reverse());
        /// <summary></summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static List<byte> UShortToListByte_BigEndian(ushort data) => new List<byte>(BitConverter.GetBytes(data).Reverse());
        /// <summary></summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static List<byte> ULongToListByte_BigEndian(ulong data) => new List<byte>(BitConverter.GetBytes(data).Reverse());
        /// <summary></summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static List<byte> DoubleToListByte_BigEndian(double data) => new List<byte>(BitConverter.GetBytes(data).Reverse());
        /// <summary></summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static List<byte> UIntToListByte_BigEndian(uint data) => new List<byte>(BitConverter.GetBytes(data).Reverse());
        /// <summary></summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static List<byte> IntToListByte_BigEndian(int data) => new List<byte>(BitConverter.GetBytes(data).Reverse());
        #endregion

        #region (De)Compress
        /// <summary>Compresses any data. Uses the zlib method to compress.</summary>
        /// <param name="tmp"></param>
        /// <returns></returns>
        public static byte[] ZlibCompress(ref byte[] tmp)
        {
            byte[] result;
            using (MemoryStream outputStream = new MemoryStream())
            {
                int bufferSize = 128 * 1024;
                var def = new Deflater(Deflater.BEST_COMPRESSION, false);

                byte[] bufferWriter = new byte[bufferSize];
                def.SetInput(tmp);
                def.Finish();

                while (!def.IsFinished)
                {
                    int outputLen = def.Deflate(bufferWriter, 0, bufferWriter.Length);
                    outputStream.Write(bufferWriter, 0, outputLen);
                }

                def.Reset();
                result = outputStream.ToArray();
            }

            return result;
        }

        /// <summary>Decompresses a data array. Uses the zlib method to compress.</summary>
        /// <param name="bufffer"></param>
        /// <returns></returns>
        public static byte[] ZlibDecompress(ref byte[] bufffer)
        {
            byte[] result;
            using (MemoryStream outputStream = new MemoryStream())
            {
                using (var compressedStream = new MemoryStream(bufffer))
                using (var inputStream = new InflaterInputStream(compressedStream))
                {
                    inputStream.CopyTo(outputStream);
                    result = outputStream.ToArray();
                }
            }

            return result;
        }
        #endregion
    }
}
