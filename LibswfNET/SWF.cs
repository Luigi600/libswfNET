﻿using LibswfNET.Tags;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;

namespace LibswfNET
{
    /// <summary></summary>
    /// TODO
    /// * Image Export: implementation different formats
    public class SWF
    {
        public List<Tag> tags = new List<Tag>();
        public byte version = new byte();
        private byte[] m_frameSize;
        private byte[] m_frameRate;
        private byte[] m_frameCount;
        private readonly Projector m_projector = new Projector();

        /// <summary>Initializes a new object of SWF.</summary>
        /// <param name="buffer">The bytes of a SWF file (typically file extensions: exe, swf).</param>
        /// <todo>Buffer than prop so it is not necessary anymore than reference via function?</todo>
        public SWF(byte[] buffer)
        {
            this.ParseSWF(buffer);
            buffer = null; //GC
        }

        #region Parsing
        /// <summary>Parses the SWF file with all data.</summary>
        /// <param name="buffer">The bytes of the SWF file.</param>
        private void ParseSWF(byte[] buffer)
        {
            byte[] swfBuf = BinaryFile2SWF(buffer);

            if (swfBuf.Length <= 4)
                throw new SWFException("Invalid SWF file. File too small.");

            int cur = ParseSWFHeader(ref swfBuf);

            //find all tags
            int tagStart = cur; // byte 21

            for (int id = 1; tagStart < swfBuf.Length; ++id)
            {
                /**
                 * We read the tag id and length from these 2 bytes which are in little-endian,
                 * since we want to put them in a bitset we order them in big-endian.
                 */
                byte[] tagCodeAndLength = new byte[] { swfBuf[++cur], swfBuf[cur - 1] };

                BitSet bs = new BitSet(tagCodeAndLength); //length of 16 because two bytes
                BitSet tagType_bs = SWF_utils.subBitset(bs, 0, 10);
                BitSet tagLength_bs = SWF_utils.subBitset(bs, 10, 6);


                /**
                 * We later export the tag code and (small) length without any calculations because:
                 * 1. We don't edit small tags
                 * 2. A long tag can have data smaller than 62 bytes and still be a long tag
                 */
                byte[] tagCodeAndLengthLE = new byte[] { swfBuf[cur - 1], swfBuf[cur] }; // save in little-endian for later export
                ETagType tagType = (ETagType)tagType_bs.ToShort();
                uint len = tagLength_bs.ToUInt();
                bool isLongTag = false;

                if (len == 0b111111)
                {
                    // Long tag
                    len = SWF_utils.bytestodec_le_UInt(swfBuf, cur + 1);
                    cur += 4;
                    isLongTag = true;
                }

                if (tagType == ETagType.DefineBinaryData)
                {
                    var dbd = new DefineBinaryData(id, SWFUtils.BytesToUShort_LittleEndian(ref swfBuf, cur + 1), isLongTag, tagCodeAndLengthLE);
                    cur += 2;
                    dbd.reserved = SWF_utils.bytestodec_le_UInt(swfBuf, +cur + 1);
                    cur += 4;
                    len -= 6;
                    // Copy tag data
                    dbd.data = new byte[len];
                    for (uint i = 0; i < len; ++i)
                        dbd.data[i] = swfBuf[++cur];
                    // Add tag to vector of tags
                    tags.Add(dbd);
                }
                else if (tagType == ETagType.DefineSound)
                {
                    var ds = new DefineSound(id, SWF_utils.bytestodec_le_UShort(swfBuf, cur + 1), isLongTag, tagCodeAndLengthLE);
                    cur += 2;
                    BitArray soundInfo = new BitArray(new byte[] { swfBuf[++cur] });
                    ds.soundFormat = SWF_utils.subBitset(soundInfo, 0, ds.soundFormat.Length);
                    ds.soundRate = SWF_utils.subBitset(soundInfo, 4, ds.soundRate.Length);
                    ds.soundSize = SWF_utils.subBitset(soundInfo, 4 + 2, ds.soundSize.Length);
                    ds.soundType = SWF_utils.subBitset(soundInfo, 4 + 2 + 1, ds.soundType.Length);
                    ds.soundSampleCount = SWF_utils.bytestodec_le_UInt(swfBuf, cur + 1);
                    cur += 4;
                    len -= 7;
                    // Copy tag data
                    ds.data = new byte[len];
                    for (uint i = 0; i < len; ++i)
                        ds.data[i] = swfBuf[++cur];
                    // Add tag to vector of tags
                    tags.Add(ds);
                }
                else if (tagType == ETagType.DefineBitsLossless || tagType == ETagType.DefineBitsLossless2)
                {
                    var dbl = new DefineBitsLossless(id, SWF_utils.bytestodec_le_UShort(swfBuf, cur + 1), isLongTag,
                        tagCodeAndLengthLE, tagType == ETagType.DefineBitsLossless2);
                    cur += 2;
                    dbl.bitmapFormat = SWF_utils.bytestodec_le_Byte(swfBuf, cur + 1); //+1 ?
                    ++cur;
                    dbl.bitmapWidth = SWF_utils.bytestodec_le_UShort(swfBuf, cur + 1);
                    cur += 2;
                    dbl.bitmapHeight = SWF_utils.bytestodec_le_UShort(swfBuf, cur + 1);
                    cur += 2;
                    if (dbl.bitmapFormat == 3)
                    {
                        dbl.bitmapColorTableSize = SWF_utils.bytestodec_le_Byte(swfBuf, cur + 1); //+1 ?
                        ++cur;
                        len--;
                    }
                    len -= 7;

                    dbl.data = new byte[len];
                    for (uint i = 0; i < len; ++i)
                        dbl.data[i] = swfBuf[++cur];

                    // Add tag to vector of tags
                    tags.Add(dbl);

                }
                else if (tagType == ETagType.SymbolClass)
                {
                    var sc = new SymbolClass(id, 0, isLongTag, tagCodeAndLengthLE);
                    sc.numSymbols = SWF_utils.bytestodec_le_UShort(swfBuf, cur + 1);
                    cur += 2;
                    for (int i = 0; i < sc.numSymbols; ++i)
                    {
                        ushort tid = SWF_utils.bytestodec_le_UShort(swfBuf, cur + 1);
                        cur += 2;
                        string name = "";
                        while (true)
                        {
                            name += (char)swfBuf[++cur];
                            if (swfBuf[cur] == 0)
                                break;
                        }
                        sc.symbolClass.Add(new Tuple<uint, string>(tid, name));
                    }

                    // Add tag to vector of tags
                    tags.Add(sc); //std::move(sc)
                }
                else
                {
                    Tag t = new Tag(id, 0, tagType, isLongTag, tagCodeAndLengthLE);

                    // Copy tag data
                    t.data = new byte[len];
                    for (uint i = 0; i < len; ++i)
                        t.data[i] = swfBuf[++cur];

                    // Add tag to vector of tags
                    tags.Add(t); //std::move(t)
                }

                /*SWF_DEBUG("Tag type: " << tagType << " (" << tagName(tagType) << ")\nTag length: " <<
                      to_string(t.length) << " bytes (" << bytesToKiB(t.length) + " KiB).");*/

                tagStart = ++cur;
            }
        }

        /// <summary>Parses the header of the SWF file, decompress the bytes and returns the read position.</summary>
        /// <param name="buffer">The compressed bytes of the SWF file (<b>Important: </b> this function supports also uncompressed SWF files).</param>
        /// <returns>The start position of the data.</returns>
        private int ParseSWFHeader(ref byte[] buffer)
        {
            int cur = 0;

            //Check if file is SWF and what compression is used
            //SWF_DEBUG_NNL("Compression: ");
            // bytes 0, 1, 2
            string signature = new string(new char[] { (char)buffer[cur], (char)buffer[++cur], (char)buffer[++cur] });

            if (signature == "FWS") // FWS is SWF in little-endian
            {
                //SWF_DEBUG("Uncompressed");
            }
            else if (signature == "CWS")
            {
                //SWF_DEBUG("zlib");
                buffer = ZlibDecompressSWF(buffer);
            }
            else if (signature == "ZWS")
            {
                //SWF_DEBUG("LZMA");
                buffer = LZMADecompressSWF(buffer);
            }
            else
                throw new SWFException("Invalid SWF file. Unrecognized header.");

            // Check version
            this.version = buffer[++cur]; // byte 3
            //SWF_DEBUG("SWF version: " << Convert.ToString(this.version));

            // Check file length
            // bytes 4, 5, 6, 7
            uint length = SWF_utils.bytestodec_le_UInt(buffer, cur + 1);
            cur += 4;
            //SWF_DEBUG("File length: " << (int)length << " bytes (" << bytesToMiB(length) << " MiB).");

            if (buffer.Length != length)
                throw new SWFException("Bytes read and SWF size don't match.");

            //SWF_DEBUG("Frame size:");
            // Nbits - number of bits used for each field of the frame size (there are 4 fields)
            BitSet nbits_bitset = new BitSet(5);
            nbits_bitset.Or(new BitArray(new int[] { buffer[++cur] >> 3 }));
            //TODO: set buffer[++cur] >> 3 to nbits_bitset // byte 8
            //bitset < 5 > nbits_bitset(buffer[++cur] >> 3); // byte 8
            int nbits = (int)SWF_utils.GetLongFromBitArray(nbits_bitset);
            //SWF_DEBUG("\tNbits: " << nbits);
            uint frameSizeBytes = (uint)((int)Math.Ceiling((((float)nbits * 4.0f) + 5.0f) / 8.0f));

            // If frame size is 9 bytes long, cur should be 17 after this
            this.m_frameSize = new byte[frameSizeBytes];
            for (uint i = 0; i < frameSizeBytes; ++i)
                this.m_frameSize[i] = buffer[cur++];

            //debugFrameSize(this.frameSize, nbits);

            this.m_frameRate = new byte[] { buffer[cur], buffer[++cur] }; // bytes 17, 18

            // Looks like 1st byte isn't used, otherwise this should be little-endian
            //SWF_DEBUG("Frame rate: " << Convert.ToString(this.frameRate[1]));

            this.m_frameCount = new byte[] { buffer[++cur], buffer[++cur] }; // bytes 19, 20

            //SWF_DEBUG("Frame count: " << bytestodec_le<ushort>(this.frameCount.data()));

            return ++cur;
        }

        /// <summary>Gets the bytes of the SWF object and sets the projector (when exists).</summary>
        /// <remarks>An exe file contains the projector. These projector has no information. The interesting part is only the SWF file.
        /// <br /><br />
        /// <b>Renamed:</b> the function name is <c>exe2swf</c> in the original C++ library.
        /// </remarks>
        /// <param name="bin">The bytes of the binar file.</param>
        /// <returns>A byte array that represents the SWF file.</returns>
        private byte[] BinaryFile2SWF(byte[] bin)
        {
            List<string> sigs = new List<string>() { "FWS", "CWS", "ZWS" };
            int swfStart = 0;
            int swfEnd = 0;
            int swfLength = 0;

            if (SWF_utils.isPEfile(bin))
            {
                int pos = 0;
                while ((pos = Search(ref bin, pos + 4, bin.Length, Projector.footer)) >= 0)
                {
                    if (bin.Length < pos + 8)
                        throw new SWFException("SWF not found inside EXE file.");

                    swfLength = SWF_utils.bytestodec_le_Int(bin, pos + 4); // <uint>(exe.data() + pos + 4);
                    if (bin.Length - pos == 8)
                        break;
                }
                if (pos < 0)
                    throw new SWFException("SWF not found inside EXE file.");

                swfStart = (bin.Length - swfLength - 8);
                swfEnd = swfStart + swfLength;

                string swfSig = new string(new char[] { (char)bin[swfStart], (char)bin[swfStart + 1], (char)bin[swfStart + 2] });

                if (!sigs.Contains(swfSig))
                    throw new SWFException("SWF not found inside EXE file.");

                this.m_projector.buffer = SWF_utils.subBytes(ref bin, 0, swfStart);
                this.m_projector.windows = true;
            }
            else if (SWF_utils.isELFfile(bin))
            {
                int pos = 0;
                while ((pos = Search(ref bin, pos + 4, bin.Length, Projector.footer)) >= 0)
                {
                    if (bin.Length < pos + 12)
                        throw new SWFException("SWF not found inside ELF file.");

                    swfLength = SWF_utils.bytestodec_le_Int(bin, pos - 4);

                    swfStart = pos + 4;
                    swfEnd = swfStart + swfLength;

                    string swfSig = new string(new char[] { (char)bin[swfStart], (char)bin[swfStart + 1], (char)bin[swfStart + 2] });

                    if (sigs.Contains(swfSig))
                        break;
                }
                if (pos < 0 || bin.Length < swfEnd)
                    throw new SWFException("SWF not found inside ELF file.");

                this.m_projector.buffer = SWF_utils.subBytes(ref bin, pos - 4, bin.Length);
                this.m_projector.windows = false;
            }
            else
                return bin;

            return SWF_utils.subBytes(ref bin, swfStart, swfEnd).ToArray();
        }
        #endregion






        public List<Tag> getTagsOfType(ETagType type)
        {
            List<Tag> tmp = new List<Tag>();
            foreach (var t in this.tags)
            {
                if (t.type == type)
                {
                    tmp.Add(t); //t.get()?
                }
            }
            return new List<Tag>(tmp);
        }

        public Tag getTagWithId(uint id)
        {
            foreach (var t in this.tags)
            {
                if (t.id == id)
                {
                    return t; //t.get() ?
                }
            }
            return null;
        }

        /// <summary>Returns the symbol name of a tag.</summary>
        /// <param name="id">The ID of the tag which one will resolve.</param>
        /// <returns>A string that representes the name when exists. Otherwise returns "Not Found".</returns>
        public string GetSymbolName(uint id)
        {
            IEnumerable<SymbolClass> tags = this.tags.OfType<SymbolClass>();
            foreach(SymbolClass symbolClass in tags)
            {
                Tuple<uint, string> name = symbolClass.symbolClass.Find(tuple => tuple.Item1 == id);
                if (name != null)
                {
                    string result = name.Item2.Trim();
                    while (result.Length > 0 && result[result.Length - 1] == 0x00)
                        result = result.Substring(0, result.Length - 1);
                    return result;
                }
                    
            }

            return "Not Found";
        }

        #region Export Functions
        /// <summary>Exports all data to a SWF file with specific compression method.</summary>
        /// <param name="compression">The compression method which should use for the export.</param>
        /// <returns>A byte array that represents the SWF file.</returns>
        public byte[] ExportSWF(ECompression compression)
        {
            List<byte> bytes = this.ToBytes();

            if (compression == ECompression.zlib)
                bytes = ZlibCompressSWF(bytes);
            else if (compression == ECompression.LZMA)
                bytes = LZMACompressSWF(bytes);
            else if (compression != ECompression.Uncompressed)
                throw new SWFException("Invalid compression option.");

            //DEBUG("SWF binary size: " << bytes.Count << " bytes (" << GlobalMembers.bytesToMiB(bytes.Count) << " MiB).");

            return bytes.ToArray();
        }

        /// <summary>Exports all data to an EXE file with specific compression method.</summary>
        /// <param name="compression">The compression method which should use for the export.</param>
        /// <returns>A byte array that represents the EXE file.</returns>
        public byte[] ExportEXE(ECompression compression) => ExportEXE(null, compression);

        /**
        * Export SWF as EXE. The binary file is as follows:
        * 1. Projector binary
        * 2. SWF binary
        * 3. Footer 0xFA123456 (little endian)
        * 4. SWF binary length
        *
        * Compression:
        * 0 - uncompressed
        * 1 - zlib
        * 2 - lzma
        *
        * TO-DO:
        * - Make function detect if projector is Windows executable or Linux ELF,
        *   and change write order accordingly.
        */
        /// <summary>Exports all data to an EXE file with specific compression method.</summary>
        /// <param name="proj">The projection file which should use.</param>
        /// <param name="compression">The compression method which should use for the export.</param>
        /// <returns>A byte array that represents the EXE file.</returns>
        public byte[] ExportEXE(byte[] proj, ECompression compression)
        {
            List<byte> bytes = this.ToBytes();
            if (compression == ECompression.zlib)
                bytes = ZlibCompressSWF(bytes);
            else if (compression == ECompression.LZMA)
                bytes = LZMACompressSWF(bytes);
            else if (compression != ECompression.Uncompressed)
                throw new SWFException("Invalid compression option.");

            // Compressed length to save alongside footer
            // so that we can calculate later the start position of the swf file
            List<byte> length = SWF_utils.dectobytes_le((uint)bytes.Count);
            if (proj != null && proj.Length > 0)
            {
                this.m_projector.buffer = new List<byte>(proj);
                if (SWF_utils.isPEfile(this.m_projector.buffer.ToArray()))
                    this.m_projector.windows = true;
                else if (SWF_utils.isELFfile(this.m_projector.buffer.ToArray()))
                    this.m_projector.windows = false;
                else
                    throw new SWFException("Invalid projector file.");
            }
            else if (!this.HasProjector())
                throw new SWFException("No projector file given.");

            // 1st comes projector
            bytes.InsertRange(0, this.m_projector.buffer);

            if (this.m_projector.windows)
            {
                // 2nd comes swf (aka original 'bytes')
                // 3rd comes projector footer
                bytes.AddRange(Projector.footer);
                // 4th comes swf length
                bytes.AddRange(length);
            }
            else
            {
                // 2nd comes swf length
                bytes.InsertRange(0, length);
                // 3rd comes projector footer
                // 4th comes swf (aka original 'bytes')
                bytes.InsertRange(0, Projector.footer);
            }

            return bytes.ToArray();
        }
        #endregion

        #region Export Tags
        /// <summary>Exports an image to a byte array when existing.</summary>
        /// <param name="imageID">The ID of the tag.</param>
        /// <returns>A byte array that represents the image in PNG format when successful. Otherwise <c>null</c>.</returns>
        public byte[] ExportImage(uint imageID)
        {
            byte[] result = null;

            IEnumerable<DefineBitsLossless> dbls = this.tags.OfType<DefineBitsLossless>().Where(dbl => dbl.id == imageID);
            foreach (DefineBitsLossless dbl in dbls)
            {
                List<byte> png = new List<byte>();
                /**
                 * DefineBitsLossless:
                 *     COLORMAPDATA for format 3
                 *     BITMAPDATA for formats 4 and 5
                 *
                 * DefineBitsLossless2:
                 *     ColorTableRGB and ColormapPixelData for format 3
                 *     ARGB[image data size] for formats 4 and 5
                 */
                byte[] decompressedImgData = SWFUtils.ZlibDecompress(ref dbl.data);

                if (dbl.type != ETagType.DefineBitsLossless2)
                {
                    throw new NotImplementedException();
                }
                else
                {
                    if (dbl.bitmapFormat == 3)
                    {
                        /**
                         * Field				Type							Comment
                         *
                         * ColorTableRGB 		RGBA[color table size]			Defines the mapping from color indices to RGBA values.
                         * 														Number of RGBA values is BitmapColorTableSize + 1.
                         *
                         * ColormapPixelData	UI8[image data size]			Array of color indices. Number of entries is BitmapWidth
                         * 														BitmapHeight, subject to padding (see note preceding
                         *														this table).
                         *
                         * In the ColorTable we have an array of RGBA possible values.
                         * The length of ColorTable in bytes is (BitmapColorTableSize + 1) * 4.
                         *
                         * The ColormapPixelData is an array of indices to ColorTableRGB whose
                         * concatenation form the image.
                         */
                        throw new NotImplementedException();
                    }
                    else
                    {
                        Bitmap bitmap = new Bitmap(dbl.bitmapWidth, dbl.bitmapHeight);
                        for (int i = 0, x = 0, y = 0; i < decompressedImgData.Length; i += 4, ++x)
                        {
                            byte a = decompressedImgData[i];
                            byte r = decompressedImgData[i + 1];
                            byte g = decompressedImgData[i + 2];
                            byte b = decompressedImgData[i + 3];
                            float alpha = decompressedImgData[i] / 255.0f;
                            r = (byte)(r / alpha);
                            g = (byte)(g / alpha);
                            b = (byte)(b / alpha);

                            if (x >= dbl.bitmapWidth)
                            {
                                ++y;
                                x = 0;
                            }

                            bitmap.SetPixel(x, y, Color.FromArgb(a, r, g, b));
                        }
                        using (MemoryStream ms = new MemoryStream())
                        {
                            bitmap.Save(ms, ImageFormat.Png);
                            result = ms.ToArray();
                        }
                    }
                }
            }

            return result;
        }

        /// <summary></summary>
        /// <param name="soundID">The ID of the tag.</param>
        /// <returns>A byte array that represents the sound in MP3 format when successful. Otherwise <c>null</c>.</returns>
        public byte[] ExportMP3(uint soundID)
        {
            IEnumerable<DefineSound> tv = this.tags.OfType<DefineSound>().Where(ds => ds.id == soundID);
            /**
             * In the SWF, MP3 data starts with a SeekSamples fields that
             * represents the number of samples to skip. It is usually 0x00 0x00
             * and we remove this because it is not part of the MP3 data.
             *
             * swf-file-format-spec.pdf - page 188
             */
            if (tv.Count() >= 0)
                return SWFUtils.SubBytes(ref tv.ElementAt(0).data, 2, tv.ElementAt(0).data.Length);

            return null;
        }

        /// <summary></summary>
        /// <param name="tagID">The ID of the tag.</param>
        /// <returns>A byte array that represents the content of the tag. Otherwise <c>null</c>.</returns>
        public byte[] ExportBinary(uint tagID)
        {
            IEnumerable<DefineBinaryData> tv = this.tags.OfType<DefineBinaryData>().Where(ds => ds.id == tagID);
            if (tv.Count() >= 0)
                return tv.ElementAt(0).data;

            return null;
        }
        #endregion

        #region Replace Tags

        //public bool ReplaceMP3(byte[] bytes, uint soundId)
        //{
        //    IEnumerable<DefineSound> tv = this.tags.OfType<DefineSound>().Where(ds => ds.id == soundId);
        //    if (tv.Count() >= 0) {
        //        //FUNCTION

        //        return true;
        //    }

        //    return false;
        //}

        public bool ReplaceBinary(byte[] bytes, uint tagId)
        {
            IEnumerable<DefineBinaryData> tv = this.tags.OfType<DefineBinaryData>().Where(ds => ds.id == tagId);
            if (tv.Count() >= 0)
            {
                tv.ElementAt(0).data = bytes;
                return true;
            }

            return false;
        }

        public bool ReplaceBinaryRAW(byte[] rawBytes, uint tagId)
        {
            byte[] compressed = SWFUtils.ZlibCompress(ref rawBytes);
            List<byte> u29 = AMF.AMFUtils.UInt29BAToList((uint) compressed.Length);
            List<byte> LMI = new List<byte>();
            LMI.Add(0x0C);

            LMI.AddRange(u29);
            LMI.AddRange(compressed);

            return ReplaceBinary(LMI.ToArray(), tagId);
        }
        #endregion

        #region (De)Compressions
        /// <summary>Compresses the data of SWF. Uses the LZMA method to compress.</summary>
        /// <param name="swf">The uncompressed data of the SWF file.</param>
        /// <returns>A byte array that represents the SWF file in LZMA compression.</returns>
        private List<byte> LZMACompressSWF(List<byte> swf)
        {
            List<byte> buffer = new List<byte>() { (byte)'Z', (byte)'W', (byte)'S', (this.version >= 13 ? this.version : (byte)13) };

            // Length
            buffer.AddRange(SWF_utils.subBytes(ref swf, 4, 8));
            List<byte> tmp = SWF_utils.subBytes(ref swf, 8, swf.Count);
            byte[] compressed = SevenZip.Compression.LZMA.SevenZipHelper.Compress(tmp.ToArray());
            // -5 because lzma properties are not included in the size
            buffer.AddRange(SWF_utils.dectobytes_le((uint)compressed.Length - 5)); //compressedSize
            buffer.AddRange(compressed);

            return buffer;
        }
        /// <summary>Decompresses the data of SWF. Uses the LZMA method to decompress.</summary>
        /// <param name="swf">The compressed data of the SWF file which are compressed in LZMA.</param>
        /// <returns>A byte array that represents the uncompressed SWF data.</returns>
        private byte[] LZMADecompressSWF(byte[] swf)
        {
            List<byte> buffer = SWF_utils.subBytes(ref swf, 0, 8);
            buffer[0] = (byte)'F';

            List<byte> tmp = SWF_utils.subBytes(ref swf, 12, swf.Length);
            byte[] decompressed = SevenZip.Compression.LZMA.SevenZipHelper.Decompress(tmp.ToArray());
            buffer.AddRange(decompressed);
            return buffer.ToArray();
        }

        //-------------- zlib --------------
        /// <summary>Compresses the data of SWF. Uses the zlib method to compress.</summary>
        /// <param name="swf">The uncompressed data of the SWF file.</param>
        /// <returns>A byte array that represents the SWF file in zlib compression.</returns>
        private List<byte> ZlibCompressSWF(List<byte> swf)
        {

            List<byte> buffer = new List<byte>() { (byte)'C', (byte)'W', (byte)'S', (this.version >= 6 ? this.version : (byte)6) };
            buffer.AddRange(SWF_utils.subBytes(ref swf, 4, 8));
            byte[] tmp = SWFUtils.SubBytes(ref swf, 8, swf.Count);

            buffer.AddRange(SWFUtils.ZlibCompress(ref tmp));
            return buffer;
        }

        /// <summary>Decompresses a data array of SWF. Uses the zlib method to compress.</summary>
        /// <param name="swf">The compressed data of the SWF file which are compressed in zlib.</param>
        /// <returns>A byte array that represents the uncompressed SWF data.</returns>
        private byte[] ZlibDecompressSWF(byte[] swf)
        {
            List<byte> buffer = SWFUtils.SubBytes2List(ref swf, 0, 8);
            buffer[0] = (byte)'F';

            byte[] tmp = SWFUtils.SubBytes(ref swf, 8, swf.Length);
            buffer.AddRange(SWFUtils.ZlibDecompress(ref tmp));

            return buffer.ToArray();
        }
        #endregion

        #region Private Stuff
        /// <summary>Returns if a projector existing.</summary>
        /// <returns>A boolean that specifices if a projector existing.</returns>
        private bool HasProjector() => m_projector.buffer.Count > 0;

        /// <summary>Returns the first occurrence position of bytes which contain in a list of bytes.</summary>
        /// <param name="searchIn">The bytes which potential contain in the searched bytes.</param>
        /// <param name="start">The start position of the search in <c>searchIn</c>.</param>
        /// <param name="end">The maximal end position of the search. As far as there the search will search in <c>searchIn</c>.</param>
        /// <param name="searchAt">The bytes which are searching.</param>
        /// <returns>The position of first occurrence (positive) when successful. Otherwise negative.</returns>
        private int Search(ref byte[] searchIn, int start, int end, List<byte> searchAt)
        {
            for (int i = start, j = 0; i < end; ++i)
            {
                if (searchIn[i] == searchAt[j])
                {
                    j++;
                    if (j == searchAt.Count)
                        return i - searchAt.Count + 1;
                }
                else
                    j = 0;
            }

            return -1;
        }

        /// <summary>Returns a byte array that represents the current SWF with all data.</summary>
        /// <returns>A byte array that represents the current SWF.</returns>
        private List<byte> ToBytes()
        {
            List<byte> buffer = new List<byte>() { (byte)'F', (byte)'W', (byte)'S', this.version };

            List<byte> buf = new List<byte>();
            foreach (var t in tags)
                buf.AddRange(t.ToBytes());

            uint length = (uint)(12 + (uint)this.m_frameSize.Length + (uint)buf.Count);

            List<byte> len = SWF_utils.dectobytes_le(length);
            buffer.AddRange(len);
            buffer.AddRange(this.m_frameSize);
            buffer.AddRange(this.m_frameRate);
            buffer.AddRange(this.m_frameCount);

            buffer.AddRange(buf);

            return buffer;
        }
        #endregion

    }
}