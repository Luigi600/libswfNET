﻿namespace LibswfNET
{
    /// <summary>
    /// Specifies the compression method for an export.
    /// </summary>
    public enum ECompression : byte
    {
        /// <summary>No compression. File output will be large.</summary>
        Uncompressed,
        /// <summary>zlib algorithm (fast).</summary>
        zlib,
        /// <summary>LZMA algorithm, very slow but the smallest output.</summary>
        LZMA
    }
}
