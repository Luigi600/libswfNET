﻿using System;
using System.Collections;

namespace LibswfNET
{
    /// <summary>BitArray replace</summary>
    public class BitSet : IEnumerable
    {
        private bool fixedLen = false;

        public IEnumerator GetEnumerator() => BitArray.GetEnumerator();

        public int Length
        {
            get => BitArray.Count;
        }

        public BitArray BitArray { get; } = null;

        public BitSet(int len)
        {
            BitArray = new BitArray(len);

            fixedLen = true;
        }

        public BitSet(int byt, bool interpretThanFixedNumber)
        {
            BitArray = new BitArray(new int[] { byt });
        }

        public BitSet(byte[] bytes)
        {
            Array.Reverse(bytes);
            BitArray = new BitArray(bytes);
        }

        #region Operators
        public bool this[int index]
        {
            get => BitArray[index];
            set => BitArray[index] = value;
        }

        public static BitSet operator <<(BitSet a, int num) //LSB is LEFT, so all one to right
        {
            for(int n = 1; n <= num; ++n) {
                for (int i = a.Length - 2; i >= 0; --i)
                    a[i + 1] = a[i]; // a[i - 1];

                a[0] = false;
            }

            return a;
        }

        public static BitSet operator >>(BitSet a, int num) //LSB is LEFT, so all one to left
        {
            for (int n = 1; n <= num; ++n) {
                for (int i = 1; i < a.Length; ++i)
                    a[i - 1] = a[i];

                a[a.Length - 1] = false;
            }

            return a;
        }

        public void Or(BitArray ba)
        {
            BitArray.Or(TrimBitArray(ba, BitArray.Length));
        }

        public static BitArray TrimBitArray(BitArray ba, int len)
        {
            BitArray result = new BitArray(len);
            for (int i = 0; i < len; ++i)
                result[i] = ba[i];

            return result;
        }
        #endregion


        #region Number Returns
        public int ToInt()
        {
            if (Length > 32)
                throw new ArgumentException("");

            return (int)GetValue();
        }

        public uint ToUInt()
        {
            if (Length > 32)
                throw new ArgumentException("");

            return (uint)GetValue();
        }

        public short ToShort()
        {
            if (Length > 16)
                throw new ArgumentException("");

            return (short)GetValue();
        }

        public long ToLong()
        {
            if (Length > 32)
                throw new ArgumentException("");

            return GetValue();
        }

        private long GetValue()
        {
            long result = 0;
            for (int i = 0, pow = 0; i < BitArray.Length; ++i, ++pow)
                result |= (long)((BitArray[i] ? 1 : 0) << pow);

            return result;
        }


        //Debug things
        public int ToIntTest() => test<int>();
        public uint ToUIntTest() => test<uint>();
        public long ToLongTest() => test<long>();
        public short ToShortTest() => test<short>();
        public ushort ToUShortTest() => test<ushort>();

        private T test<T>() where T : struct
        {
            int size = System.Runtime.InteropServices.Marshal.SizeOf(default(T));
            if (Length > size)
                throw new ArgumentException("");

            return (dynamic) GetValue();
        }
        #endregion


        public byte[] GetFixedLengthByteArray(int len)
        {
            byte[] result = new byte[len];
            BitArray.CopyTo(result, 0);

            return result;
        }

        public override string ToString()
        {
            string result = "";
            for (int i = BitArray.Length - 1; i >= 0; --i)
                result += BitArray[i] ? "1" : "0";

            return result;
        }
    }
}