﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace LibswfNET
{
    //public static class ListExtension
    //{
    //    public static void Emplace<S>(this IList<S> list, params object[] parameters)
    //    {
    //        list.Add((S)Activator.CreateInstance(typeof(S), parameters));
    //    }
    //}

    static class SWF_utils
    {
        public static string ToString(this System.Collections.BitArray bitArray)
        {
            string result = "";
            foreach(bool booool in bitArray)
                result += booool ? "1" : "0";

            return result;
        }
        public static uint bytesToMiB(uint bytes)
        {
            return bytes / 1024 / 1024;
        }
        public static uint bytesToKiB(uint bytes)
        {
            return bytes / 1024;
        }
        public static int twipsToPx(int twips)
        {
            return twips / 20;
        }

        public static BitArray subBitset(BitArray bs, int start_pos, int length)
        {
            BitArray result = new BitArray(length);
            for(int i = bs.Length - 1 - start_pos,
                j = length - 1;
                i >= bs.Length - length - start_pos && j >= 0;
                i--, j--)
            {
                result[j] = bs[i];
            }

            return result;
        }

        public static BitSet subBitset(BitSet bs, int start_pos, int length)
        {
            BitSet result = new BitSet(length);
            for (int i = bs.Length - 1 - start_pos,
                j = length - 1;
                i >= bs.Length - length - start_pos && j >= 0;
                i--, j--)
            {
                result[j] = bs[i];
            }

            return result;
        }

        public static List<byte> subBytes(ref List<byte> bytes, int start, int end)
        {
            if (end > bytes.Count || end < start)
                end = bytes.Count;

            List<byte> result = new List<byte>();
            for (int i = start; i < end; ++i)
                result.Add(bytes[i]);

            return result;
        }
        public static List<byte> subBytes(ref byte[] bytes, int start, int end)
        {
            if (end > bytes.Length || end < start)
                end = bytes.Length;

            List<byte> result = new List<byte>();
            for (int i = start; i < end; ++i)
                result.Add(bytes[i]);

            return result;
        }
        public static byte[] getByteParts(byte[] bytes, int seek)
        {
            if (seek >= bytes.Length)
                throw new Exception("");

            byte[] result = new byte[bytes.Length - seek];
            for (int i = seek; i < bytes.Length; ++i)
                result[i - seek] = bytes[i];
            return result;
        }

        public static uint bytestodec_le_UInt(byte[] bytes, int seek)
        {
            //byte[] tmp = new byte[sizeof(int)];
            //int start = seek;
            //int iEnd = seek + tmp.Length - 1;

            //for (int i = iEnd; i >= start; --i)
            //    tmp[i - start] = bytes[seek + (iEnd - i)];

            ////bytes = getByteParts(bytes, seek);
            return BitConverter.ToUInt32(bytes, seek);
        }

        public static int bytestodec_le_Int(byte[] bytes, int seek) => BitConverter.ToInt32(bytes, seek);
        public static ushort bytestodec_le_UShort(byte[] bytes, int seek) => BitConverter.ToUInt16(bytes, seek);
        public static byte bytestodec_le_Byte(byte[] bytes, int seek) => bytes[seek];

        public static ushort bytestodec_UShort(byte[] bytes, int seek) {
            Array.Reverse(bytes);
            return BitConverter.ToUInt16(bytes, seek);
        }

        /// <summary>Look on a specific range of a byte array and reverse this range (Little-Endian).</summary>
        /// <param name="bytes"></param>
        /// <param name="seek"></param>
        /// <param name="len"></param>
        //private static getBytesReversed(byte[] bytes, int seek, int len)
        //{
        //    byte[] tmp = new byte[sizeof(int)];
        //    int start = seek;
        //    int iEnd = seek + tmp.Length - 1;

        //    for (int i = iEnd; i >= start; --i)
        //        tmp[i - start] = bytes[seek + (iEnd - i)];

        //    bytes = getByteParts(bytes, seek);
        //}

        public static bool isPEfile(byte[] exe)
        {
            if (exe.Length < 0x3C + 4)
                return false;

            string exeSig = new string(new char[] { (char)exe[0], (char)exe[1] });
            if (exeSig.Equals("MZ")) // Mark Zbikowski
            {
                //int test = (int)bytestodec_le_UInt(getByteParts(exe.ToArray(), 0x3C));
                int pointerPE = (int) bytestodec_le_UInt(exe, 0x3C);
                if (exe.Length < pointerPE + 4)
                    return false;

                exeSig = new string(new char[] { (char)exe[pointerPE], (char)exe[pointerPE + 1] });
                return exeSig.Equals("PE");
            }
            else
                return false;
        }

        public static bool isELFfile(byte[] exe)
        {
            if (exe.Length < 4)
                return false;

            string exeSig = new string(new char[] {(char)exe[1], (char)exe[2], (char)exe[3]});
            return (exe[0] == (byte)0x7F && exeSig.Equals("ELF"));
        }


        /**
 * Image files
 * https://en.wikipedia.org/wiki/Portable_Network_Graphics
 */
        public static bool isPNGfile(List<byte> png)
        {
            byte[] magicNumber = { 0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A };
            if (png.Count < magicNumber.Length)
                return false;

            for (int i = 0; i < magicNumber.Length; ++i)
                if (magicNumber[i] != png[i])
                    return false;

            return true;
        }
        public static bool isJPEGfile(List<byte> jpeg)
        {
            byte[] header = { 0xFF, 0xD8 };
            byte[] footer = { 0xFF, 0xD9 };
            if (jpeg.Count < header.Length)
                return false;
            for (int i = 0; i < header.Length; ++i)
                if (header[i] != jpeg[i])
                    return false;

            int iStart = jpeg.Count - 2;
            for (int i = iStart; i < jpeg.Count; ++i)
                if (jpeg[i] != footer[i - iStart])
                    return false;

            return true;
        }
        public static bool isGIFfile(List<byte> gif)
        {
            byte[] magicNumber = { 0x47, 0x49, 0x46, 0x38 };
            if (gif.Count < magicNumber.Length)
                return false;

            for (int i = 0; i < magicNumber.Length; i++)
                if (magicNumber[i] != gif[i])
                    return false;

            return true;
        }



        //public static List<byte> dectobytes_le<T>(List<T> d)
        //{
        //    List<byte> bytes = new List<byte>(d.Count);

        //    for (int i = 0; i < d.Count; i++) // sizeof(T); ++i)
        //    {
        //        byte tmp = 0;
        //        tmp |= (byte)d;
        //        bytes[i] = tmp;
        //        d = (T)(d >> 8);
        //    }

        //    return new List<byte>(bytes);
        //}
        public static List<byte> dectobytes_le(uint data)
        {
            byte[] bytes = BitConverter.GetBytes(data);
            return new List<byte>(bytes);
        }

        public static List<byte> dectobytes_le(int data)
        {
            byte[] bytes = BitConverter.GetBytes(data);
            return new List<byte>(bytes);
        }

        public static List<byte> dectobytes_le(short data)
        {
            byte[] bytes = BitConverter.GetBytes(data);
            return new List<byte>(bytes);
        }

        public static List<byte> dectobytes_le(ushort data)
        {
            byte[] bytes = BitConverter.GetBytes(data);
            return new List<byte>(bytes);
        }

        public static List<byte> dectobytes_le(byte data)
        {
            byte[] bytes = BitConverter.GetBytes(data);
            return new List<byte>(bytes);
        }

        public static long GetLongFromBitArray(BitSet bs)
        {
            return BitConverter.ToInt64(bs.GetFixedLengthByteArray(sizeof(long)), 0);
        }
        public static short GetUIntFromBitArray(BitSet bs)
        {
            return BitConverter.ToInt16(bs.GetFixedLengthByteArray(sizeof(short)), 0);
        }

        public static long GetIntFromBitArray(BitArray ba)
        {
            byte[] bytes = new byte[sizeof(long)];
            ba.CopyTo(bytes, 0);
            return BitConverter.ToInt64(bytes, 0);
        }
        public static short GetUIntFromBitArray(BitArray ba)
        {
            byte[] bytes = new byte[sizeof(short)];
            ba.CopyTo(bytes, 0);
            return BitConverter.ToInt16(bytes, 0);
        }

        public static ushort bytestodec_be_UShort(byte[] bytes, int seek)
        {
            ushort result = 0;
            for (int i = 0; i < sizeof(ushort); ++i)
                result = (ushort) (result | bytes[seek++] << ((sizeof(ushort) - i - 1) * 8));

            return result;
        }

        public static uint bytestodec_be_UInt(byte[] bytes, int seek)
        {
            uint result = 0;
            for (int i = 0; i < sizeof(uint); ++i)
                result = (result | (uint) bytes[seek++] << ((sizeof(uint) - i - 1) * 8));

            return result;
        }
    }
}
