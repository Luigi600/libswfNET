﻿using System.Collections;
using System.Collections.Generic;

namespace LibswfNET
{
    namespace Tags
    {
        public class DefineSound : Tag
        {
            private static SortedDictionary<int, string> codingFormats = new SortedDictionary<int, string> {
                { 0, "Uncompressed, native-endian"},
	            { 1, "ADPCM"},
	            { 2, "MP3"},
	            { 3, "Uncompressed, little-endian"},
	            { 4, "Nellymoser 16 kHz"},
	            { 5, "Nellymoser 8 kHz"},
	            { 6, "Nellymoser"},
	            { 7, "Speex"}
            };

            private static SortedDictionary<int, int> soundRates = new SortedDictionary<int, int> {
                { 0, 5512},
	            { 1, 11025},
	            { 2, 22050},
	            { 3, 44100}
            };

            private static SortedDictionary<int, string> soundRatesNames = new SortedDictionary<int, string> {
                { 0, "5512 Hz"},
	            { 1, "11025 Hz"},
	            { 2, "22050 Hz"},
	            { 3, "44100 Hz"}
            };

            public uint soundSampleCount;
            public BitArray soundFormat = new BitArray(4);
            public BitArray soundRate = new BitArray(2);
            public BitArray soundSize = new BitArray(1);
            public BitArray soundType = new BitArray(1);

            public DefineSound(int i, uint id, bool longTag, byte[] tagCodeAndLength) : base(i, id, ETagType.DefineSound, longTag, tagCodeAndLength)
            {}

            public static string formatName(int f)
            {
                return (!codingFormats.ContainsKey(f)) ? "Unknown" : codingFormats[f];
            }
            public static string soundRateName(int f)
            {
                return (!soundRatesNames.ContainsKey(f)) ? "Unknown" : soundRatesNames[f];
            }

            internal override List<byte> ToBytes()
            {
                List<byte> buffer = new List<byte>();
                buffer.AddRange(this.tagCodeAndLength);

                if (this.longTag)
                {
                    // Long tag
                    //List<byte> len = SWF_utils.dectobytes_le((uint)this.data.Length + 2 + 1 + 4); // sound id + sound info + sample count
                    buffer.AddRange(SWFUtils.UIntToBytes_LittleEndian((uint) (this.data.Length + 2 + 1 + 4)));
                }

                //List<byte> sid = SWF_utils.dectobytes_le((ushort)this.id);
                buffer.AddRange(SWFUtils.UShortToBytes_LittleEndian((ushort) this.id));

                var soundInfo = (byte)(
                    (SWF_utils.GetIntFromBitArray(this.soundFormat) << 4) |
                    (SWF_utils.GetIntFromBitArray(this.soundRate) << 2) |
                    (SWF_utils.GetIntFromBitArray(this.soundSize) << 1) |
                    (SWF_utils.GetIntFromBitArray(this.soundType) << 0)
                );

                buffer.Add(soundInfo);

                //List<byte> sc = SWF_utils.dectobytes_le(this.soundSampleCount);
                buffer.AddRange(SWFUtils.UIntToBytes_LittleEndian(this.soundSampleCount));

                buffer.AddRange(this.data);

                return buffer;
            }
        }
    }
}
