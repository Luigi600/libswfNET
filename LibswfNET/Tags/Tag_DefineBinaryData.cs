﻿using System.Collections.Generic;

namespace LibswfNET
{
    namespace Tags
    {
        public class DefineBinaryData : Tag
        {
            // tagId - 2 bytes
            public uint reserved = 0; // must be 0

            public DefineBinaryData(int i, uint id, bool longTag, byte[] tagCodeAndLength) : base(i, id, ETagType.DefineBinaryData, longTag, tagCodeAndLength)
            { }

            internal override List<byte> ToBytes()
            {
                List<byte> buffer = new List<byte>();
                buffer.AddRange(this.tagCodeAndLength);

                if (this.longTag)
                {
                    // Long tag
                    // character id (2) + reserved (4)
                    List<byte> len = SWF_utils.dectobytes_le((uint)this.data.Length + 2 + 4);
                    buffer.AddRange(len);
                }

                //List<byte> cid = SWF_utils.dectobytes_le((ushort)this.id);
                buffer.AddRange(SWFUtils.UShortToBytes_LittleEndian((ushort) id));

                //List<byte> res = SWF_utils.dectobytes_le(this.reserved);
                buffer.AddRange(SWFUtils.UIntToBytes_LittleEndian(reserved));

                buffer.AddRange(this.data);

                return buffer;
            }
        }
    }
}