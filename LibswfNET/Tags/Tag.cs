﻿using System.Collections.Generic;

namespace LibswfNET
{
    namespace Tags
    {
        public class Tag
        {
            public readonly int i = 0;
            public readonly uint id = 0;
            public readonly ETagType type;
            public readonly bool longTag = false;
            public readonly byte[] tagCodeAndLength; // we don't need to manipulate this as we don't mess with short tags
            public byte[] data = new byte[] {};

            public Tag(int i,
                       uint id,
                       ETagType type,
                       bool longTag,
                       byte[] tagCodeAndLength)
            {
                this.i = i;
                this.id = id;
                this.type = type;
                this.longTag = longTag;
                this.tagCodeAndLength = tagCodeAndLength;
            }

            internal virtual List<byte> ToBytes()
            {
                List<byte> buffer = new List<byte>();
                buffer.AddRange(this.tagCodeAndLength);

                if (this.longTag)
                    buffer.AddRange(SWFUtils.UIntToBytes_LittleEndian((uint) data.Length)); // SWF_utils.dectobytes_le((uint)this.data.Length));

                buffer.AddRange(this.data);
                return buffer;
            }

            public override string ToString() => type.ToString() + " - ID: " + id;
        }
    }
}