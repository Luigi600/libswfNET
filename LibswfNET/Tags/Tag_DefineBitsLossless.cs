﻿using System.Collections.Generic;

namespace LibswfNET
{
    namespace Tags
    {
        class DefineBitsLossless : Tag
        {
            public byte bitmapFormat;
            public ushort bitmapWidth;
            public ushort bitmapHeight;
            public byte bitmapColorTableSize; //if bitmapFormat = 3, otherwise absent

            public DefineBitsLossless(int i, uint id, bool longTag, byte[] tagCodeAndLength, bool isVersion2) : base(i,
                id, isVersion2 ? ETagType.DefineBitsLossless2 : ETagType.DefineBitsLossless, longTag, tagCodeAndLength)
            { }

            internal override List<byte> ToBytes()
            {
                List<byte> buffer = new List<byte>();
                buffer.AddRange(this.tagCodeAndLength);

                if (this.longTag)
                {
                    // Long tag
                    // character id (2) + bitmapFormat (1) + bitmapWidth (2) + bitmapHeight (2) + bitmapColorTableSize (1)
                    //List<byte> len = SWF_utils.dectobytes_le((uint)(this.data.Length + 2 + 1 + 2 + 2 + (this.bitmapFormat == 3 ? 1 : 0)));
                    buffer.AddRange(SWFUtils.UIntToBytes_LittleEndian((uint) (this.data.Length + 2 + 1 + 2 + 2 + (this.bitmapFormat == 3 ? 1 : 0))));
                }

                //List<byte> cid = SWF_utils.dectobytes_le((ushort)this.id);
                buffer.AddRange(SWFUtils.UShortToBytes_LittleEndian((ushort) this.id));

                buffer.Add(this.bitmapFormat);

                //List<byte> bw = SWF_utils.dectobytes_le((ushort)this.bitmapWidth);
                buffer.AddRange(SWFUtils.UShortToBytes_LittleEndian((ushort)this.bitmapWidth));

                //List<byte> bh = SWF_utils.dectobytes_le((ushort)this.bitmapHeight);
                buffer.AddRange(SWFUtils.UShortToBytes_LittleEndian((ushort)this.bitmapHeight));

                if (this.bitmapFormat == 3)
                    buffer.Add(this.bitmapColorTableSize);

                buffer.AddRange(this.data);
                return buffer;
            }
        }
    }
}