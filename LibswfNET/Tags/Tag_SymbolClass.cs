﻿using System;
using System.Collections.Generic;

namespace LibswfNET
{
    namespace Tags
    {
        public class SymbolClass : Tag
        {
            public ushort numSymbols;
            public List<Tuple<uint, string>> symbolClass = new List<Tuple<uint, string>>();

            public SymbolClass(int i, uint id, bool longTag, byte[] tagCodeAndLength) : base(i, id, ETagType.SymbolClass, longTag, tagCodeAndLength) { }

            internal override List<byte> ToBytes()
            {
                List<byte> buffer = new List<byte>();
                buffer.AddRange(this.tagCodeAndLength);

                List<byte> content = new List<byte>();
                foreach (var p in this.symbolClass)
                {
                    //List<byte> tid = SWF_utils.dectobytes_le((ushort)p.Item1);
                    content.AddRange(SWFUtils.UShortToBytes_LittleEndian((ushort) p.Item1));
                    content.AddRange(new System.Text.ASCIIEncoding().GetBytes(p.Item2));
                }

                //List<byte> len = SWF_utils.dectobytes_le((uint)content.Count + 2); // numSymbols
                //List<byte> numSym = SWF_utils.dectobytes_le((ushort)this.symbolClass.Count); //dectobytes_le<uint16_t>(this->numSymbols);

                buffer.AddRange(SWFUtils.UIntToBytes_LittleEndian((uint) content.Count + 2));
                buffer.AddRange(SWFUtils.UShortToBytes_LittleEndian((ushort) symbolClass.Count));
                buffer.AddRange(content);

                return buffer;
            }
        }
    }
}