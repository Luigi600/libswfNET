﻿using System.Collections.Generic;

namespace LibswfNET
{
    internal class Projector
    {
        public bool windows = false;
        public List<byte> buffer = new List<byte>();
        public static readonly List<byte> footer = new List<byte>() { 0x56, 0x34, 0x12, 0xFA };

        public Projector() {} //dummy
    }
}
