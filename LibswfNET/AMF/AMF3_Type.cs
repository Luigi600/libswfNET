﻿using System.Collections.Generic;

namespace LibswfNET.AMF
{
    public class AMF3_Type
    {
        /***
         * AMF3:
         * undefined-marker = 0x00
         * null-marker = 0x01
         * false-marker = 0x02
         * true-marker = 0x03
         * integer-marker = 0x04
         * double-marker = 0x05
         * string-marker = 0x06
         * xml-doc-marker = 0x07
         * date-marker = 0x08
         * array-marker = 0x09
         * object-marker = 0x0A
         * xml-marker = 0x0B
         * byte-array-marker = 0x0C
         * vector-int-marker = 0x0D
         * vector-uint-marker = 0x0E
         * vector-double-marker = 0x0F
         * vector-object-marker = 0x10
         * dictionary-marker = 0x11
         */
        public byte type = 0;
        public uint u29LenSize = 0; // Length in bytes of the U29 variable length
        public byte[] data = new byte[] {};
    }
}
