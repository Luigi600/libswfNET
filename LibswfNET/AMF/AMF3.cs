﻿using System;
using System.Collections.Generic;

namespace LibswfNET.AMF
{
    public class AMF3
    {
        public List<AMF3_Type> objects = new List<AMF3_Type>();

        public AMF3(byte[] buffer)
        {
            int pos = 0;

            /**
			 * byte-array-marker = 0x0C
			 * See section 3.1 of amf3-file-format-spec.pdf
			 */
            if (buffer[pos] == 0x0C)
            {

                if (buffer.Length < 5)
                {
                    throw new SWFException("Cannot interpret this AFM3 file.");
                }
                ++pos;

                /**
				 * Then comes a variable-length of the ByteArray
				 * See section 1.3.1 of amf-file-format-spec.pdf
				 */
                uint len = AMFUtils.ReadUInt29(ref buffer, ref pos);
                /**
				 * Reads a 29-bit AMF3 byte-length header and gets rid of
				 * the last bit which is a flag and always 1 for ByteArray (which is the case)
				 *
				 * Example:
				 *    Binary: 0011000 0100111 1001111
				 *    Decimal: 398287
				 * Becomes:
				 *    Binary: 0011000 0100111 100111
				 *    Decimal: 199143
				 *
				 * 199143 is the length in bytes of the compressed file in
				 * './415_Data.Global_livermoreLmi.bin'
				 *
				 * See section 3.14 of amf-file-format-spec.pdf
				 */
                len >>= 1;

                AMF3_Type at = new AMF3_Type();
                at.type = 0x0C;
                at.u29LenSize = (uint)(pos - 1); // don't count the type as part of the length
                at.data = SWFUtils.SubBytes(ref buffer, pos, pos + (int) len);

                objects.Add(at);
            }
            /**
			 * null-marker = 0x01
			 * See section 3.3 of amf3-file-format-spec.pdf
			 */
            else if (buffer[pos] == 0x01)
            {
                AMF3_Type at = new AMF3_Type();
                at.type = 0x01;
                at.u29LenSize = 0;
                objects.Add(at);
            }
            else
            {
                //std::stringstream stream = new std::stringstream();
                //stream << std::hex << setw(2) << setfill('0') << (int)buffer[pos];
                throw new SWFException("Position: " + Convert.ToString(pos) + ". Marker '0x" + "' not valid or not implemented.");
            }

        }
    }
}
