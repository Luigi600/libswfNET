﻿using System;
using System.Collections.Generic;

namespace LibswfNET.AMF
{
    public class AMFUtils
    {
        internal static double ReadDouble(byte[] buffer, int pos)
        {
            byte[] bytes = SWFUtils.SubBytes(ref buffer, pos, pos + sizeof(double));
            Array.Reverse(bytes);
            return BitConverter.ToDouble(bytes, 0);
        }

        internal static double ReadDouble(ICollection<byte> buffer, int pos)
        {
            byte[] bytes = SWFUtils.SubBytes(ref buffer, pos, pos + sizeof(double));
            Array.Reverse(bytes);
            return BitConverter.ToDouble(bytes, 0);
        }

        public static void WriteStringWithLenPrefixU16(ref List<byte> data, string s)
        {
            data.AddRange(SWFUtils.UShortToBytes_BigEndian((ushort)s.Length));
            data.AddRange(Array.ConvertAll(s.ToCharArray(), item => (byte)item));
        }

        /**
         * Takes 7 bits from each byte.
         * Returns before reading 4 bytes if the byte it is
         * reading does not start with bit 1
         *
         * Example:
         *     Hex: 98 A7 4F
         *     Binary: 10011000 10100111 01001111
         *
         *     The first 2 bytes start with bit 1 so we keep
         *     reading until the 3rd byte that starts with bit
         *     0. We remove the first bit of each byte, so it becomes:
         *     Binary: 0011000 0100111 1001111
         *     Decimal: 398287
         *
         * See section 1.3.2 of amf-file-format-spec.pdf
         */
        public static uint ReadUInt29(ref byte[] buffer, ref int pos)
        {
            uint len = 0;
            for (int i = pos, max = pos + 3; i < max; ++i)
            {
                if (buffer[i] < 0x80)
                {
                    len |= buffer[i];
                    ++pos;
                    break;
                }
                else
                {
                    len = (len | ((uint)buffer[i] & 0x7f)) << 7;
                    if (i == max - 1)
                    {
                        len <<= 1;
                        len |= buffer[i + 1];
                        ++pos;
                    }
                    ++pos;
                }
            }
            return len;
        }

        public static List<byte> UInt29BAToList(uint num) //original name "Uint29BAToVec"
        {
            BitSet bs = new BitSet((int)num, false);
            bs <<= 1;
            bs[0] = true; // ByteArray has last bit set to 1

            // Count number of bits needed
            uint count = 0;
            if (num != 0)
            {
                for (int i = (int)(bs.Length - 1); i >= 0; --i)
                {
                    if (!bs[i])
                        ++count;
                    else
                        break;
                }
            }
            else
                count = 32;

            count = (uint)(32 - count);

            List<byte> bytes = new List<byte>();
            if (count <= 7)
            { // 1 byte - 0xxxxxxx
                bytes.Add((byte)SWF_utils.GetLongFromBitArray(bs));// (byte)bs.to_ulong());
            }
            else if (count <= 7 * 2)
            { // 2 bytes - 1xxxxxxx 0xxxxxxx
                BitSet first = new BitSet(8);
                BitSet sec = new BitSet(8);
                for (int i = 0; i < 7; ++i)
                    sec[i] = bs[i];

                for (int i = 7; i < 14; ++i)
                    first[i - 7] = bs[i];

                first[7] = true;
                bytes.Add((byte)SWF_utils.GetLongFromBitArray(first));
                bytes.Add((byte)SWF_utils.GetLongFromBitArray(sec));
            }
            else if (count <= 7 * 3)
            { // 3 bytes - 1xxxxxxx 1xxxxxxx 0xxxxxxx
                BitSet first = new BitSet(8);
                BitSet sec = new BitSet(8);
                BitSet third = new BitSet(8);

                for (int i = 0; i < 7; ++i)
                    third[i] = bs[i];

                for (int i = 7; i < 14; ++i)
                    sec[i - 7] = bs[i];

                for (int i = 14; i < 21; ++i)
                    first[i - 14] = bs[i];

                first[7] = true;
                sec[7] = true;
                bytes.Add((byte)SWF_utils.GetLongFromBitArray(first));
                bytes.Add((byte)SWF_utils.GetLongFromBitArray(sec));
                bytes.Add((byte)SWF_utils.GetLongFromBitArray(third));
            }
            else if (count <= 7 * 3 + 8)
            { // 4 bytes - 1xxxxxxx 1xxxxxxx 1xxxxxxx xxxxxxxx
                BitSet first = new BitSet(8);
                BitSet sec = new BitSet(8);
                BitSet third = new BitSet(8);
                BitSet fourth = new BitSet(8);

                for (int i = 0; i < 8; ++i)
                    fourth[i] = bs[i];

                for (int i = 8; i < 15; ++i)
                    third[i - 8] = bs[i];

                for (int i = 15; i < 22; ++i)
                    sec[i - 15] = bs[i];

                for (int i = 22; i < 29; ++i)
                    first[i - 22] = bs[i];

                first[7] = true;
                sec[7] = true;
                third[7] = true;
                bytes.Add((byte)SWF_utils.GetLongFromBitArray(first));
                bytes.Add((byte)SWF_utils.GetLongFromBitArray(sec));
                bytes.Add((byte)SWF_utils.GetLongFromBitArray(third));
                bytes.Add((byte)SWF_utils.GetLongFromBitArray(fourth));
            }
            else
                throw new SWFException("Number '" + Convert.ToString(num) + "' has more than 29 bits.");

            return new List<byte>(bytes);
        }
    }
}
