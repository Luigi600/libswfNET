﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibswfNET.AMF
{
    public class AMF0
    {
        private JObject jsonObject = new JObject();

        private enum AMF0Marker : byte
        {
            TypedObject = 0x10,
            ObjectMarker = 0x03,
            ObjectEnd = 0x09,
            Null = 0x05,
            Undefined = 0x06,
            Number = 0x00,
            Boolean = 0x01,
            String = 0x02,
            ECMAArray = 0x08
        }

        public AMF0(ICollection<byte> buffer)
        {
            // Buffer position
            int pos = 0;
            if (buffer.Count() > 0 && buffer.ElementAt(pos) == 0x10)
            {
                ++pos;
                jsonObject.Add(ReadPropertyName(ref buffer, ref pos), new JObject());
                //// Get class name
                //ushort len = SWF_utils.bytestodec_be_UShort(buffer, pos);
                //pos += 2;

                //List<char> chars = new List<char>();
                //for (int i = pos; i < pos + len; ++i)
                //    chars.Add((char) buffer[i]);

                //string cn = new string(chars.ToArray());
                //pos += len;
                //jsonObject.Add(cn, new JObject());

                this.ParseAMF0(buffer, ref pos, jsonObject.Last.Last);

                // Creates a key with the class name and an object as value
                //this.parseAMF0(buffer, ref pos, @object[cn]);
            }
            else
                throw new SWFException("Sorry, only serialization of objects implemented.");
        }

        private void ParseAMF0(ICollection<byte> buffer, ref int pos, JToken obj)
        {
            string varName = "";
            bool readVarName = false;

            while (pos < buffer.Count())
            {
                if (!readVarName)
                {
                    // Get variable name
                    varName = ReadPropertyName(ref buffer, ref pos);

                    readVarName = true;
                    continue;
                }

                // --> readVarName == true <--
                AMF0Marker marker = (AMF0Marker) buffer.ElementAt(pos);

                if(Enum.IsDefined(typeof(AMF0Marker), marker))
                {
                    readVarName = false;
                    ++pos;

                    /// typed-object-marker = 0x10
                    /// See section 2.18 of amf0-file-format-specification.pdf
                    if (marker == AMF0Marker.TypedObject)
                    {
                        // Creates a key with the class name and an object as value
                        JObject j = new JObject();
                        j["HFW_classNameXXX"] = ReadPropertyName(ref buffer, ref pos); // XXX way I found to have a named object
                        AddToJSON(obj, varName, j);

                        this.ParseAMF0(buffer, ref pos, j);
                    }

                    /// object-marker = 0x03
                    /// See section 2.5 of amf0-file-format-specification.pdf
                    else if (marker == AMF0Marker.ObjectMarker)
                    {
                        JObject j = new JObject();
                        AddToJSON(obj, varName, j);
                        // Creates a key with the class name and an object as value

                        this.ParseAMF0(buffer, ref pos, j);
                    }



                    /// object-end-marker = 0x09
                    /// at the end of typed object, anonymous object and array
                    /// See section 2.11 of amf0-file-format-specification.pdf
                    else if (marker == AMF0Marker.ObjectEnd)
                        return;



                    /// null-marker = 0x05
                    /// See section 2.7 of amf0-file-format-specification.pdf
                    else if (marker == AMF0Marker.Null)
                        AddToJSON(obj, varName, null);



                    /// undefined-marker = 0x06
                    /// See section 2.8 of amf0-file-format-specification.pdf
                    else if (marker == AMF0Marker.Undefined)
                        AddToJSON(obj, varName, "HFW_undefinedXXX");  // XXX Don't know a better way to distinguish from null
                    


                    /// number-marker = 0x00
                    /// See section 2.2 of amf0-file-format-specification.pdf
                    else if (marker == AMF0Marker.Number)
                    {
                        double d = AMFUtils.ReadDouble(buffer, pos);

                        /**
                         * NaN can have many different representations, and
                         * infinite can have 2 representations (positive and negative),
                         * so we store an array of bytes for them to keep their
                         * exact representation.
                         */
                        if (!double.IsInfinity(d) && !double.IsNaN(d))
                            AddToJSON(obj, varName, d);
                        else
                        {
                            //TODO: IMPLEMENTATION
                            ////obj[varName]; //wtf
                            //for (uint i = 0; i < 8; ++i)
                            //{
                            //    ((JObject)obj[varName]).Add(buffer[pos + i]);
                            //}
                        }

                        pos += 8;
                    }
                    /// boolean-marker = 0x01
                    /// See section 2.3 of amf0-file-format-specification.pdf
                    else if (marker == AMF0Marker.Boolean)
                        AddToJSON(obj, varName, (bool)(buffer.ElementAt(pos++) != 0));



                    /// string-marker = 0x02
                    /// See section 2.4 of amf0-file-format-specification.pdf
                    else if (marker == AMF0Marker.String)
                        AddToJSON(obj, varName, ReadPropertyName(ref buffer, ref pos));



                    /// ecma-array-marker = 0x08
                    /// See section 2.11 of amf0-file-format-specification.pdf
                    else if (marker == AMF0Marker.ECMAArray)
                    {
                        //uint aLen = SWF_utils.bytestodec_be_UInt(buffer, pos);
                        pos += 4;

                        /**
                         * ECMA arrays work like objects except they are prefixed with a length.
                         * But sometimes this length is zero and the array contains objects,
                         * I don't know why.
                         */
                        //obj[varName]; //wtf
                        JArray jArr = new JArray();
                        AddToJSON(obj, varName, jArr);
                        //obj[varName]["HFW_ArrayLenXXX"] = aLen;
                        this.ParseAMF0(buffer, ref pos, jArr);
                    }
                }
                else
                {
                    //std::stringstream stream = new std::stringstream();
                    //stream << std::hex << setw(2) << setfill('0') << (int)buffer[pos];
                    throw new SWFException("Position: " + Convert.ToString(pos) + ". Marker '0x" + "' not valid or not implemented."); //+ stream.str()
                }
            }
        }

        public static byte[] FromJSON(string js)
        {
            List<byte> bytes = new List<byte>();
            JObject obj = JObject.Parse(js);

            // Typed object
            bytes.Add((byte) AMF0Marker.TypedObject);

            // class name's length
            string key = ((JProperty)obj.First).Name;
            AMFUtils.WriteStringWithLenPrefixU16(ref bytes, key);

            foreach (var el in obj.First.First.Children<JProperty>())
            {
                //JProperty[] props = el.Properties().ToArray();
                AMFUtils.WriteStringWithLenPrefixU16(ref bytes, el.Name);
                ParseJSONElement(ref bytes, el.Value);
            }

            //end of object
            bytes.AddRange(new byte[] { 0x00, 0x00, 0x09 });

            return bytes.ToArray();
        }

        private static void ParseJSONElement(ref List<byte> buffer, JToken el)
        {
            if (el.Type == JTokenType.Null)
                buffer.Add((byte) AMF0Marker.Null);



            else if (el.Type == JTokenType.Boolean)
            {
                buffer.Add((byte) AMF0Marker.Boolean);
                if (el.Value<bool>() == true)
                    buffer.Add(0x01);
                else
                    buffer.Add(0x00);
            }
            else if (el.Type == JTokenType.Float || el.Type == JTokenType.Integer)
            {
                buffer.Add((byte)AMF0Marker.Number);
                buffer.AddRange(SWFUtils.DoubleToBytes_BigEndian(el.Value<double>()));
            }
            else if (el.Type == JTokenType.Object)
            {
                JToken classNameXXX = FindJObject(el, "HFW_classNameXXX");
                if (classNameXXX != null && classNameXXX != el.Last && classNameXXX.Type == JTokenType.String)
                {
                    buffer.Add((byte)AMF0Marker.TypedObject);
                    string className = el["HFW_classNameXXX"].Value<string>();
                    AMFUtils.WriteStringWithLenPrefixU16(ref buffer, className);
                }
                else
                    buffer.Add((byte)AMF0Marker.ObjectMarker);

                foreach (var el2 in el.Children<JProperty>())
                {
                    if (el2.Name == "HFW_classNameXXX" || el2.Name == "HFW_ArrayLenXXX")
                        continue;

                    AMFUtils.WriteStringWithLenPrefixU16(ref buffer, el2.Name);
                    ParseJSONElement(ref buffer, el2.Value);
                }

                buffer.AddRange(new byte[] { 0x00, 0x00, 0x09 });
            }
            else if (el.Type == JTokenType.String)
            {
                string s = el.Value<string>();
                if (s == "HFW_undefinedXXX")
                    buffer.Add(0x06);
                else
                {
                    buffer.Add(0x02);
                    AMFUtils.WriteStringWithLenPrefixU16(ref buffer, s);
                }
            }
            else if (el.Type == JTokenType.Array)
            {
                buffer.Add((byte)AMF0Marker.ECMAArray);
                JArray jArray = (JArray) el;
                buffer.AddRange(SWFUtils.UIntToBytes_BigEndian((uint)jArray.Count));

                int counter = 0;
                foreach (var test in jArray.Children())
                {
                    AMFUtils.WriteStringWithLenPrefixU16(ref buffer, (counter++).ToString());
                    ParseJSONElement(ref buffer, test);
                }
                buffer.AddRange(new byte[] { 0x00, 0x00, 0x09 });

                //if (el.Count == 8) //el.size()
                //{ // Non-finite number
                //    buffer.Add(0x00);
                //    //buffer.AddRange(el);
                //}
                //else
                //    throw new SWFException("JSON Arrays only in use for non-finite numbers.");
            }
            else
                throw new SWFException("Unrecognized JSON type.");
        }

        private static string ReadPropertyName(ref ICollection<byte> buffer, ref int pos)
        {
            ushort len = SWFUtils.BytesToUShort_BigEndian(ref buffer, pos);
            pos += 2;

            char[] bytes = new char[len];
            for (int i = pos; i < pos + len; ++i)
                bytes[i - pos] = (char)buffer.ElementAt(i);

            pos += len;

            return new string(bytes);
        }

        private static void AddToJSON(JToken jObject, string propName, object value)
        {
            if (jObject is JContainer)
                if (jObject is JArray)
                    ((JArray)jObject).Add(value);
                else
                    ((JContainer)jObject).Add(new JProperty(propName, value));
            else
                throw new SWFException("");
        }

        private static JToken FindJObject(JToken jObject, string name)
        {
            foreach(JToken jObj in jObject.Children())
            {
                //if (jObj.Property("Name").Name == name)
                //    return jObj;
            }

            return null;
        }

        /// <summary>ToJSON</summary>
        /// <returns></returns>
        public override string ToString() => jsonObject.ToString();
    }
}