﻿using LibswfNET;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace LibswfNETTests
{
    [TestClass]
    public class Converter_LE
    {
        [TestMethod]
        public void UIntToBytes() => CollectionAssert.AreEqual(new byte[] { 5, 0, 0, 0}, SWFUtils.UIntToBytes_LittleEndian((uint)5));

        [TestMethod]
        public void UIntToBytesNegative() => CollectionAssert.AreEqual(new byte[] { 251, 255, 255, 255 }, SWFUtils.UIntToBytes_LittleEndian(unchecked((uint)-5)));

        [TestMethod]
        public void IntToBytes() => CollectionAssert.AreEqual(new byte[] { 5, 0, 0, 0 }, SWFUtils.IntToBytes_LittleEndian((int)5));

        [TestMethod]
        public void IntToBytesNegative() => CollectionAssert.AreEqual(new byte[] { 251, 255, 255, 255 }, SWFUtils.IntToBytes_LittleEndian((int)-5));

        [TestMethod]
        public void ShortToBytes() => CollectionAssert.AreEqual(new byte[] { 5, 0 }, SWFUtils.ShortToBytes_LittleEndian((short)5));

        [TestMethod]
        public void ShortToBytesNegative() => CollectionAssert.AreEqual(new byte[] { 251, 255 }, SWFUtils.ShortToBytes_LittleEndian((short)-5));

        [TestMethod]
        public void UShortToBytes() => CollectionAssert.AreEqual(new byte[] { 5, 0 }, SWFUtils.UShortToBytes_LittleEndian((ushort)5));

        [TestMethod]
        public void UShortToBytesNegative() => CollectionAssert.AreEqual(new byte[] { 251, 255 }, SWFUtils.UShortToBytes_LittleEndian(unchecked((ushort)-5)));

        [TestMethod]
        public void UIntToListByte() => CollectionAssert.AreEqual(new List<byte>(new byte[] { 5, 0, 0, 0 }), SWFUtils.UIntToListByte_LittleEndian((uint)5));

        [TestMethod]
        public void UIntToListByteNegative() => CollectionAssert.AreEqual(new List<byte>(new byte[] { 251, 255, 255, 255 }), SWFUtils.UIntToListByte_LittleEndian(unchecked((uint)-5)));

        [TestMethod]
        public void IntToListByte() => CollectionAssert.AreEqual(new List<byte>(new byte[] { 5, 0, 0, 0 }), SWFUtils.IntToListByte_LittleEndian((int)5));

        [TestMethod]
        public void IntToListByteNegative() => CollectionAssert.AreEqual(new List<byte>(new byte[] { 251, 255, 255, 255 }), SWFUtils.IntToListByte_LittleEndian((int)-5));

        [TestMethod]
        public void ShortToListByte() => CollectionAssert.AreEqual(new List<byte>(new byte[] { 5, 0 }), SWFUtils.ShortToListByte_LittleEndian((short)5));

        [TestMethod]
        public void ShortToListByteNegative() => CollectionAssert.AreEqual(new List<byte>(new byte[] { 251, 255 }), SWFUtils.ShortToListByte_LittleEndian((short)-5));

        [TestMethod]
        public void UShortToListByte() => CollectionAssert.AreEqual(new List<byte>(new byte[] { 5, 0 }), SWFUtils.UShortToListByte_LittleEndian((ushort)5));

        [TestMethod]
        public void UShortToListByteNegative() => CollectionAssert.AreEqual(new List<byte>(new byte[] { 251, 255 }), SWFUtils.UShortToBytes_LittleEndian(unchecked((ushort)-5)));
    }
}