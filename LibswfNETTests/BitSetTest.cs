using Microsoft.VisualStudio.TestTools.UnitTesting;
using LibswfNET;

namespace LibswfNETTests
{
    [TestClass]
    public class BitSetTest
    {
        BitSet bs = new BitSet(new byte[] { 20 });

        [TestMethod]
        public void FixedLengthLength() => Assert.AreEqual(8, bs.GetFixedLengthByteArray(8).Length);

        [TestMethod]
        public void FixedLength()
        {
            byte[] bytes = bs.GetFixedLengthByteArray(8);
            //System.Diagnostics.Trace.Write(string.Join(',', bytes));
            CollectionAssert.AreEqual(new byte[] { 20, 0, 0, 0, 0, 0, 0, 0 }, bytes); //its byte array NOT a bit
        }

        [TestMethod]
        public void BitsToString() => Assert.AreEqual("00010100", bs.ToString());

        [TestMethod]
        public void Value() => Assert.AreEqual(20, bs.ToInt());

        [TestMethod]
        public void ValueLong() => Assert.AreEqual((long) 20, bs.ToLong());

        [TestMethod]
        public void ValueLongTest() => Assert.AreEqual((long)20, bs.ToLongTest());

        [TestMethod]
        public void LeftShift()
        {
            bs <<= 2;
            Assert.AreEqual("01010000", bs.ToString());
        }

        [TestMethod]
        public void RightShift()
        {
            bs >>= 2; //leftShift has no influence on bs?????
            Assert.AreEqual("00000101", bs.ToString());
        }
    }
}