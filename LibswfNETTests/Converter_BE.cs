﻿using LibswfNET;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace LibswfNETTests
{
    [TestClass]
    public class Converter_BE
    {
        [TestMethod]
        public void UIntToBytes() => CollectionAssert.AreEqual(new byte[] { 0, 0, 0, 5 }, SWFUtils.UIntToBytes_BigEndian((uint)5));

        [TestMethod]
        public void UIntToBytesNegative() => CollectionAssert.AreEqual(new byte[] { 255, 255, 255, 251 }, SWFUtils.UIntToBytes_BigEndian(unchecked((uint)-5)));

        [TestMethod]
        public void IntToBytes() => CollectionAssert.AreEqual(new byte[] { 0, 0, 0, 5 }, SWFUtils.IntToBytes_BigEndian((int)5));

        [TestMethod]
        public void IntToBytesNegative() => CollectionAssert.AreEqual(new byte[] { 255, 255, 255, 251 }, SWFUtils.IntToBytes_BigEndian((int)-5));

        [TestMethod]
        public void ShortToBytes() => CollectionAssert.AreEqual(new byte[] { 0, 5 }, SWFUtils.ShortToBytes_BigEndian((short)5));

        [TestMethod]
        public void ShortToBytesNegative() => CollectionAssert.AreEqual(new byte[] { 255, 251 }, SWFUtils.ShortToBytes_BigEndian((short)-5));

        [TestMethod]
        public void UShortToBytes() => CollectionAssert.AreEqual(new byte[] { 0, 5 }, SWFUtils.UShortToBytes_BigEndian((ushort)5));

        [TestMethod]
        public void UShortToBytesNegative() => CollectionAssert.AreEqual(new byte[] { 255, 251 }, SWFUtils.UShortToBytes_BigEndian(unchecked((ushort)-5)));

        [TestMethod]
        public void UIntToListByte() => CollectionAssert.AreEqual(new List<byte>(new byte[] { 0, 0, 0, 5 }), SWFUtils.UIntToListByte_BigEndian((uint)5));

        [TestMethod]
        public void UIntToListByteNegative() => CollectionAssert.AreEqual(new List<byte>(new byte[] { 255, 255, 255, 251 }), SWFUtils.UIntToListByte_BigEndian(unchecked((uint)-5)));

        [TestMethod]
        public void IntToListByte() => CollectionAssert.AreEqual(new List<byte>(new byte[] { 0, 0, 0, 5 }), SWFUtils.IntToListByte_BigEndian((int)5));

        [TestMethod]
        public void IntToListByteNegative() => CollectionAssert.AreEqual(new List<byte>(new byte[] { 255, 255, 255, 251 }), SWFUtils.IntToListByte_BigEndian((int)-5));

        [TestMethod]
        public void ShortToListByte() => CollectionAssert.AreEqual(new List<byte>(new byte[] { 0, 5 }), SWFUtils.ShortToListByte_BigEndian((short)5));

        [TestMethod]
        public void ShortToListByteNegative() => CollectionAssert.AreEqual(new List<byte>(new byte[] { 255, 251 }), SWFUtils.ShortToListByte_BigEndian((short)-5));

        [TestMethod]
        public void UShortToListByte() => CollectionAssert.AreEqual(new List<byte>(new byte[] { 0, 5 }), SWFUtils.UShortToListByte_BigEndian((ushort)5));

        [TestMethod]
        public void UShortToListByteNegative() => CollectionAssert.AreEqual(new List<byte>(new byte[] { 255, 251 }), SWFUtils.UShortToListByte_BigEndian(unchecked((ushort)-5)));

    }
}