﻿# LibswfNET
LibswfNET is the .NET version of the `libswf` library to edit each SWF file (or exe file which contains the projector and SWF). The `libswf` library is created by MangaD.
The .NET version is created by Luigi600.

## Remarks
This library is a translated version of the original C++ code into C#. **It is not a C++/CLI Wrapper.**

The first version equals the original C++ code. Most of the properties are public (no encapsulation) and a lot of `List<byte>` and `List<byte>.ToArray()`. 
So the second version will get encapsulation, renames of few functions and more consistency of types.

The first version is supposed to show only that the library is working.

<br />

## Dependencies
In the case that the library is not working directly: add follow packages via NuGet:
 * [ICSharpCode.SharpZipLib - NuGet](https://www.nuget.org/packages/ICSharpCode.SharpZipLib)
 * [Newtonsoft.Json](https://www.nuget.org/packages/Newtonsoft.Json/)

<br />

## Links
 * [libswf C++ library](https://gitlab.com/MangaD/libswf) created by MangaD
 * [Hero Fighter Workshop](https://gitlab.com/MangaD/hf-workshop) (uses libswf) created by MangaD
 * [7ZIP LZMA SDK](https://7-zip.org/sdk.html)

<br />

## ToDo
 - [ ] Encapsulation
 - [ ] Documentation
 - [x] Performance
 - [ ] Cleanup `List<byte>` and `byte[]`
 - [x] Export function
 - [ ] Import function
 - [x] Parsing JSON
 - [ ] Different image formats support
 - [ ] Examples
 - [ ] Unit tests

<br />

## Idea of This Project
To develop front ends the .NET framework its easier than in C++. I want create an IDE for a game which uses SWF. In this case I'm more efficient with this library.

<br />

## Credits
 * MangaD: original libray